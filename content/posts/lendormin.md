---
title: '戀多眠Lendormin'
date: Wed, 03 Oct 2012 04:34:42 +0000
draft: false
categories: [安眠藥]
tags: [失眠]
---

Lendormin戀多眠是屬於BZD類的短效型安眠藥，同類的有Dormicum 導眠靜，Halcion 酣樂欣，對於中樞神經系統的接受體具特別的結合力，及高親和力，可縮短入眠時間，和減少覺醒次數還有增加睡眠時間． 

Lendormin 中文名稱為戀多眠，成分為Brotizolam 
適應症：失眠症的治療 

Lendormin口服吸收以後可完全在胃腸道吸收，在一小時內可達到最高血中濃度，生體可用率為70%，蛋白質結合率約為90%．Lendormin主要在肝臟代謝，半衰期由3.1~8.4小時，肝硬化的病人需要調整劑量． 

副作用：白天嗜睡、頭痛、眩暈、運動失調、複視，此現象治療一段時間會消失。 注意事項：

1.  突然停藥會增加戒斷症狀或反彈現象，應採用漸減方式停藥。
2.  會引起近事健忘．
3.  老年人及腎功能不全的病人需減量
4.  因其有呼吸抑制的功能，所以慢性呼吸功能不全的病人需小心使用
5.  較高劑量或與酒精併服，記憶力可能受損。
6.  和[Ketoconazole](http://www.tcdruginfo.com/My%20Webs%5CKetoconazole.html)並用要非常小心