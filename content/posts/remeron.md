---
title: '樂活優Remeron'
date: Mon, 28 Jan 2013 05:41:28 +0000
draft: false
categories: [安眠藥]
tags: [失眠, 抗憂鬱劑, 食慾增加]
---

在精神科的分類中，Remeron是一種[抗憂鬱劑](https://www.hi29.net/tags/%e6%8a%97%e6%86%82%e9%ac%b1%e5%8a%91/)，可以在憂鬱症狀發作時作為治療之用。為tetracyclic piperazinoazepine類抗憂鬱劑，化學結構與三環抗憂鬱劑，單胺氧化酶抑制劑，SSRIs都不同。Remeron是作用在中樞突觸前拮抗α2-adrenergic receptors，增加正腎上腺素及血清素（serotonin）的活性。Remeron的抗組織胺H1作用會導致其鎮靜效果。 Remeron 中文名稱為 樂活優，成分為 mirtazapine

適應症 ：憂鬱症

在口服Remeron以後，口服濃度在兩小時後達到最高，和身體的蛋白質結合率大約85%，平均半衰期20到40小時，藥物在服用三四天後達到穩定狀態。食物並不會影響Remeron樂活優的吸收狀態。Remeron在肝臟功能不全或者是腎臟功能不全的病人必須調整劑量。

副作用：困倦、頭暈、口乾、[便秘](http://www.liverx.net/tag/%E4%BE%BF%E7%A7%98)、食慾增加、體重增加

注意事項：

1.  Remeron 口溶錠請保持手部乾燥，撕開包裝後應立刻服用。
2.  未使用前請保持密封。
3.  使用本藥可能出現短暫性眩暈，當平躺坐起或起身站立時宜放慢動作。
4.  接受任何手術或拔牙前請告知醫師或牙醫師正在服用本藥。