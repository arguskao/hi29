---
title: '酣樂欣Halcion'
date: Wed, 01 Feb 2012 04:48:42 +0000
draft: false
categories: [安眠藥]
tags: [BZD, 失眠, 眼壓上升]
---

在精神科藥物裡面，Halcion 酣樂欣是屬於BZD類的短效型[安眠藥](https://www.hi29.net/tags/%e5%ae%89%e7%9c%a0%e8%97%a5/)，同類的有Dormicum 導眠靜，[Lendormin戀多眠](https://www.hi29.net/2012/10/%e6%88%80%e5%a4%9a%e7%9c%a0lendormin/)；除治療失眠外，也有解焦慮，肌肉鬆弛和解痙性質。

口服後約15~30分鐘效果出現，屬於一種相當短效的BZD．

Halcion 中文名稱為酣樂欣 

成分為 triazolam 

適應症：[失眠](https://www.hi29.net/tags/%e5%a4%b1%e7%9c%a0/) Halcion口服吸收快速，大概一到兩個小時可以達到血中最高濃度，約90%與血漿蛋白結合， 91%由尿排出，半衰期約為2~3小時． 

副作用：

1.  鎮靜，嗜眠，視力障礙
2.  幻覺，夢行症，順行性健忘
3.  方向感缺失，不切實際，焦躁不安，暴躁易怒

注意事項

1.  Halcion的長期使用，可能會導致這些藥物的生理或心理性依賴。
2.  不可任意停藥，易產生戒斷症狀的發生。包括頭痛、肌肉疼痛、極度焦慮、緊張、、幻覺甚至癲癇發作。
3.  使用本藥不可同時飲用酒精，會增加副作用的影響，因此建議治療期間勿飲酒。
4.  對於急性狹角性青光眼之患者投與要非常小心(恐會使眼壓上升)
5.  對於重症肌無力症之患者投與要非常小心(由於肌肉弛緩作用恐會使症狀惡化)
6.  有中等程度或嚴重的呼吸功能不全的患者引起呼吸功能不全，造成CO2昏迷

以下藥品為學名藥：

[妥利安寧](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01027474)、[定爾安](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01028149)、[得雅靜](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01032042)、[瑞汎眠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01032803)、[舒眠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01033390)、[利眠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01034896)、[好安眠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01034952)、[安林](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01031054)、[使立眠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01033959)

 

下一篇[幫助睡眠的食物](https://www.hi29.net/2012/01/%e5%b9%ab%e5%8a%a9%e7%9d%a1%e7%9c%a0%e7%9a%84%e9%a3%9f%e7%89%a9/)