---
title: 安定文Ativan
date: Sun, 04 Mar 2012 14:43:30 +0000
categories:
- 安眠藥
tags:
- BZD
- 安眠藥

---
Ativan 中文名稱"安定文 "，成分Lorazepam，屬於中效BZD 有0.5mg，1mg，2mg。非精神科病患通常起始劑量為0.5mg。

Ativan口服非常容易吸收，投藥後約兩小時就可以達到血中最高濃度。在人體的血漿中，半衰期約在12\~18小時，也就是排出體內需要60\~108小時左右。會有85%與蛋白質結合，在身體裡面與尿酸結合成不具活性的尿酸化合物，是Ativan主要代謝的途徑。

Ativan沒有活性代謝物，所以血中濃度和投藥劑量呈正比，即使投藥六個月以上，也不會有堆積的問題產生。 Ativan的代謝和Cytochrome P450關係不大，臨床上也和年齡的影響不大，所以不必因為老年人而調整劑量；對於肝功能不好的病人也不用調整劑量，**但是對於腎臟功能不好的病人則需要特別注意!!**

適應症：焦慮狀態

注意事項 ：

1. 病人如果有急性狹角性青光眼，嚴重呼吸功能不足或重肌無力症時，需格外留意
2. 於肝或腎功能不良的患者須採取一般注意事項
3. 用此類藥品會產生短暫性健忘或者記憶缺失的報告
4. 長期服用時，最好檢查患者的肝功能指數和血球
5. 如果治療憂鬱引起的焦慮，需要注意病患有自殺的可能性

如果拿到以下的藥也是一樣的學名藥 SILENCE[悠然錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01019247)， LORAZEPAM[樂得靜錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01019495)， STAPAM[速立安錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01022211)， LORAZIN[樂力靜錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01022816)， ANZEPAM[安祈平錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01023370)， ANXIEDIN[安靜錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01024463)，ANTIAPM[得定平錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01021495)、WINTIN[溫鎮錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01022630)、LORAZEPAM[樂立舒平錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01022815)、FULETIN[服能靜錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01023257)、LORAT[樂拉特錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01023342)、LORAZEPAM[安樂寧錠](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01024384)、

下一篇[歸脾湯](https://www.hi29.net/2012/02/%e6%ad%b8%e8%84%be%e6%b9%af/)