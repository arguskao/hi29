---
title: '贊安諾Xanax'
date: Thu, 16 Feb 2012 23:50:39 +0000
draft: false
categories: [安眠藥]
tags: [BZD, 失眠, 抗憂鬱劑]
---

[BZD](https://www.hi29.net/tags/bzd/)類安眠藥還有一個常見的藥物，那就是[xanax](http://www.liverx.net/tag/xanax)，中文叫做贊安諾，劑量有三種0.25mg和0.5mg還有1mg，臨床上常用的是0.5mg；成分是Alprazolam，一般常用來治療恐慌症主要是一種抗焦慮的藥物(並非抗憂鬱的藥物)。

常見的副作用為嗜睡 ((所以也有醫生拿來治療[失眠](https://www.hi29.net/tags/%e5%a4%b1%e7%9c%a0/)的病人))、協調能力降低，嚴重的副作用為暴躁、衝動、健忘、暴力行為 

如果有以下中文，也是一樣的學名藥 [景安寧](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01041465)、[健得靜](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01042514)、[柔安](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01042537)、[煩力平](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01044546)、[安柏寧](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01044685)、[歐普樂](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01045070)、[安寶寧](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01047961)、[愛伯樂](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=02020640)、[克焦慮](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=02021033) 

中文: 贊安諾 學名: Alprazolam 

衛生署許可適應症：焦慮狀態 **作用：舒緩焦慮，肌肉鬆弛** 

副作用：嗜睡、警覺性及動作協調能力降低、口乾、小便困難、視覺模糊、嘔吐、頭痛、…等 

注意事項： 1、 因為可能會有思睡、頭昏眼花、視覺模糊的現象，故服藥後應避免開車或操作危險機械 2、 使用此藥超過四星期則不可貿然停藥 3、 **因為可能會有成癮性，除非經過醫師審慎評估 後，否則勿使用超過4個月** 

下一篇[酣樂欣Halcion](https://www.hi29.net/2012/02/%e9%85%a3%e6%a8%82%e6%ac%a3halcion/)