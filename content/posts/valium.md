---
title: '煩寧Valium'
date: Wed, 18 Apr 2012 02:31:53 +0000
draft: false
categories: [安眠藥]
tags: [BZD, 失眠]
---

這個藥大概是我第一個認識的鎮靜[安眠藥](http://www.liverx.net/tag/安眠藥)了，還記得十幾年以前，安眠藥還沒有管制的時代，一顆好像只要一塊錢，也沒有人可以靠賣這東西賺取暴利（無能的衛生署將這藥列入管制，根本是圖利賣假藥的藥局），雖然到處可以買的到，不過自殺率並沒有特別高，（白癡！！要死有很多種方法，不一定要吃安眠藥，把經濟搞好比較重要！）。

不過說了這麼多，還是要來簡單的介紹一下這個藥！！ 1963年，具有代表性的一個藥品Valium上市了！嚴格說來Valium（煩寧），應該是屬於安眠藥才對，安眠藥有三種分類，而Valium（煩寧） 是屬於其中的BZD類藥品，Valium屬於其中的長效藥品，半衰期達20小時到50小時， 會在身體裡面產生累積，即使到了第二天仍然會有昏睡的感覺！ 

如果你拿到的是以下的藥品，那也是一樣的成分 DIAPIN[當立平](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01000059)，TRANZEPAM[可助寧](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01000895)，DIAZEPAM[維靜](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01001217)， VALISIN[倍利靜](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01001413)， DIAZEPAM[丹祁屏](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01001646)、JINLUN[靜朗](http://www.fda.gov.tw/licnquery/DO8180T.asp?Type=Lic&LicId=01002244)、 成分：DIAZEPAM 適應症：焦慮狀態、[失眠](http://www.liverx.net/tag/%e5%a4%b1%e7%9c%a0/)、肌肉痙攣。 副作用：腳步笨拙或不穩，頭暈，昏昏欲睡，口齒不清，食慾改變．   

下一篇[天王補心丹](https://www.hi29.net/2012/04/%e5%a4%a9%e7%8e%8b%e8%a3%9c%e5%bf%83%e4%b8%b9/)